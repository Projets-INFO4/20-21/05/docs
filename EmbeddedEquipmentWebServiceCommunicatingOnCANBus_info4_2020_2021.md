# Follow-up sheet Project 5: Embedded equipment web service communicating on CAN bus by Liam ANDRIEUX, Lucas DREZET and Roman REGOUIN

## Team:
**Tutor**: Didier DONSEZ<br>
**Students**: Liam ANDRIEUX, Lucas DREZET & Roman REGOUIN<br>
**Department**: INFO4 - Polytech Grenoble<br>

## Repositories:
- [docs](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/docs "Respository docs")
- [Documentation](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/Documentation "Documentation")
- [Raspberry](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/raspberry "Raspberry"): Raspberry code (**OUTDATED**)
- [STM32F4 simulateur CAN bus](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F4_CAN "STM32F4 CAN"): CAN bus simulator
- [STM32F7 CycloneTCP](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_CycloneServer "STM32F7 CycloneTCP"): websocket web server (**FINAL VERSION**) and ajax web server (**OUTDATED**)
- [STM32F7 Berkeley Socket](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_BerkeleySocketsServer "STM32F7 BerkeleySocket"): first embedded web server (**OUTDATED**)
- [STM32F7 Minnow](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_MinnowServer "STM32F7 Minnow"): websocket web server (**ABORTED**)
- [STM32F7 Mongoose](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_MongooseServer "STM32F7 Mongoose"): websocket web server (**ABORTED**)


___
## Week 1 (01/02/2021):
- **General part:**
    - Weekly meeting with M. DONSEZ for the project monitoring
    - Started up and got used to the Raspberry PI 3B, the STM32F779I-EVAL and the STM32H747I-EVAL
- **STM32 part:**
    - Installation of STM32CubeIDE and all STM32's basic software
___
## Week 2 (08/02/2021):
- **General part:**
    - Weekly meeting with M. DONSEZ for the project monitoring
    - 2 meetings with the IESE students to talk about the progress of the project and discuss the future of it
    - Meeting with the IESE students and M. DONSEZ to clarify some aspects
- **Raspberry part:**
    - Installed [Apache](https://github.com/apache/httpd "Apache Github") on the Raspberry and connected an external device on this website
    - Installed [Node.js](https://github.com/nodejs/node "Node.js Github") on the Raspberry and started coding a basic websocket (see [Raspberry Repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/raspberry "Raspberry Repository"))
- **STM32 part:**
    - Continued to learn how to use the STM32 tools and the different boards
    - Started searching for websockets for STM32 boards
___
## Week 3 (15/02/2021 - Holidays):
- **Raspberry part:**
    - Random data generation on a raspberry server and first basic client interface (see [Raspberry Repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/raspberry "Raspberry Repository"))
- **STM32 part:**
    - Started looking for various existant embedded server using websockets:
        - [Minnow Server - FreeRTOS](https://github.com/RealTimeLogic/MinnowServer "Minnow Github") (Open Source (GPL-2.0) and Commercial Licenses)
        - [Mongoose - FreeRTOS](https://github.com/cesanta/mongoose "Mongoose Github") (Open Source (GPL-2.0) and Commercial Licenses)
        - [Barracuda Server - FreeRTOS](https://realtimelogic.com/products/barracuda-web-server/ "Barracuda Website") (Commercial License)
        - [Mongoose OS - available on various STM32 cards (including STM32F4, STM32F7)](https://github.com/cesanta/mongoose-os "Mongoose OS Github") (Open Source (Apache 2.0) and Commercial Licenses)
        - [CycloneTCP Embedded IPv4 / IPv6 Stack - FreeRTOS](https://www.oryx-embedded.com/products/CycloneTCP "CycloneTCP Website")  (Open Source (GPL-2.0), evaluation and Commercial Licenses)
___
## Week 4 (22/02/2021):
- **General part:**
    - Weekly meeting with M. DONSEZ for the project monitoring
    - Looked for documentation about CAN, embedded servers
    - Looked for how issues on Gitlab works to use them during the project and started to create some
- **STM32 part:**
    - Started creating a basic embedded server on STM32F779I-EVAL board (see [STM32F7 Repository - Berkeley version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_BerkeleySocketsServer "STM32F7 Repository - Berkeley version"))
___
## Week 5 (01/03/2021):
- **General part:**
    - Weekly meeting with M. DONSEZ for the project monitoring
- **Raspberry part:**
    - Finished the raspberry version 1.0 webserver: first UI, decimal data, filters (see [Raspberry Repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/raspberry "Raspberry Repository"))
    - Started (and almost finished) the raspberry version 1.1 webserver: hexadecimal data (see [Raspberry Repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/raspberry "Raspberry Repository"))
- **STM32 part:**
    - Succeeded in running a web server on STM32F7 boards (not using WebSockets but Berkeley Sockets) (see [STM32F7 Repository - Berkeley version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_BerkeleySocketsServer "STM32F7 Repository - Berkeley version"))
    - Started to add 2 libraries which will allow us to use WebSockets on STM32F7 boards ([Mongoose](https://github.com/cesanta/mongoose "Mongoose Github") and [Minnow](https://github.com/RealTimeLogic/MinnowServer "Minnow Github")) (see [STM32F7 Repository - Mongoose version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_MongooseServer/-/tree/IntegrateMongoose "STM32F7 Repository - Mongoose version") and [STM32F7 Repository - Minnow version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_MinnowServer/-/tree/IntegrateMinnowServer "STM32F7 Repository - Minnow version"))
___
## Week 6 (08/03/2021):
- **General part:**
    - [Pre-viva](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/docs/-/blob/master/Pre%20viva%20presentation.pdf "Pre-viva presentation")
    - Started documents's interpretation
- **Raspberry part:**
    - Finished raspberry version 1.1 (see [Raspberry Repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/raspberry "Raspberry Repository"))
    - Added functionnality to send information from client to server (see [Raspberry Repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/raspberry "Raspberry Repository"))
- **STM32 part:**
    - Added [CycloneTCP](https://www.oryx-embedded.com/products/CycloneTCP "CycloneTCP Website")'s library on STM32F7 boards which allows us to use WebSockets (compiles but does not work) (see [STM32F7 Repository - CycloneTCP version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_CycloneServer "STM32F7 Repository - CycloneTCP version"))
    - Continued to add [Minnow](https://github.com/RealTimeLogic/MinnowServer "Minnow Github")'s library on STM32F7 boards (see [STM32F7 Repository - Minnow version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_MinnowServer/-/tree/IntegrateMinnowServer "STM32F7 Repository - Minnow version"))
___
## Week 7 (15/03/2021):
- **General part:**
    - Weekly meeting with M. DONSEZ for the project monitoring
- **Raspberry part:**
    - Started a graphical interface of the elevator (see [Raspberry Repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/raspberry "Raspberry Repository"))
- **STM32 part:**
    - Debugged STM32F7's project [CycloneTCP](https://www.oryx-embedded.com/products/CycloneTCP "CycloneTCP Website")'s library one (see [STM32F7 Repository - CycloneTCP version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_CycloneServer "STM32F7 Repository - CycloneTCP version"))
    -  Continued to add [Minnow](https://github.com/RealTimeLogic/MinnowServer "Minnow Github")'s library one (see [STM32F7 Repository - Minnow version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_MinnowServer/-/tree/IntegrateMinnowServer "STM32F7 Repository - Minnow version"))
___
## Week 8 (22/03/2021):
- **General part:**
    - Weekly meeting with M. DONSEZ for the project monitoring
- **Raspberry part:**
    - Continued graphical interface of the elevator (see [Raspberry Repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/raspberry "Raspberry Repository"))
    - Added commands on the server side to manipulate the graphical representation of the elevator on the client side (see [Raspberry Repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/raspberry "Raspberry Repository"))
- **STM32 part:**
    - Succeeded in running a web server on STM32F7 (using AJAX) (see [STM32F7 Repository - CycloneTCP version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_CycloneServer/-/tree/CANSnifferAJAX "STM32F7 Repository - CycloneTCP version"))
    - Succeeded in running a web server on STM32F7 (using websockets) (see [STM32F7 Repository - CycloneTCP version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_CycloneServer/-/tree/master "STM32F7 Repository - CycloneTCP version"))
    - Added IESE3's CAN Sniffer code to AJAX web server project and succeeded to communicate between CAN Sniffer, our server and the sending to clients (see [STM32F7 Repository - CycloneTCP version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_CycloneServer/-/tree/CANSnifferAJAX "STM32F7 Repository - CycloneTCP version"))
    - Added IESE3's CAN Sniffer code to websocket web server project (not yet tested to communicate between CAN Sniffer, our server and the sending to clients) (see [STM32F7 Repository - CycloneTCP version, websocket and CAN Sniffer version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_CycloneServer/-/tree/master "STM32F7 Repository - CycloneTCP version, websocket and CAN Sniffer version"))
    - Continued to add [Minnow](https://github.com/RealTimeLogic/MinnowServer "Minnow Github")'s library by adding [JSON](https://github.com/RealTimeLogic/JSON "JSON's library from creators of Minnow")'s library and [SMQ](https://github.com/RealTimeLogic/SMQ "SMQ's library from creators of Minnow")'s library from [Minnow](https://github.com/RealTimeLogic/MinnowServer "Minnow Github") creators (see [STM32F7 Repository - Minnow version](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_MinnowServer/-/tree/IntegrateMinnowServer "STM32F7 Repository - Minnow version"))
___
## Week 9 (29/03/2021):
- **General part:**
    - Weekly meeting with M. DONSEZ for the project monitoring
    - Tested our programs in the real elevator at GEII
- **Raspberry part:**
    - Continued UI
- **STM32 part:**
    - Ported client code from Raspberry to [STM32F7 CycloneTCP project](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_CycloneServer "Repository STM32F7 CycloneTCP project")
    - Fixed bugs
    - Added new view client side
    - Added help for each parts of the website (navbar, sketch, table 1 with all data and table 2 with data per floor)
    - Changed whole client side code (html, CSS, js)
    - Used and modified [Arduino MCP2515 Project](https://github.com/autowp/arduino-mcp2515 "MCP2515 Repository") to simulate a CAN Bus for our systems (Open Source (MIT)) (see [STM32F4 Repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F4_CAN "STM32F4 Repository"))
___
## Week 10 (05/04/2021):
- **General part:**
    - Final viva
    - Tested our programs in the real elevator at GEII
- **STM32 part:**
    - Fix bugs and new CSS